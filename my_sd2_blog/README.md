#  TP : My SD² Blog 

# Sujet

Ce tp est un challenge d'implémentation de la partie infra / pipeline automation d'un site web type "blog personnel" en JamStack.

# Mise en route

> Ce TP n'est **pas** un challenge de développement frontend. Nous vous fournissons un site Hugo préconfiguré, avec un thème basique qui permet d'afficher le contenu. Vous êtes néanmoins libre de toucher la config Hugo comme vous l'entendez.




Ajouter le theme hugo:
```
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke my_sd2_blog/themes/ananke
```

Si vous voulez lancer le site dans un container docker local:
```
docker run -p 1313:1313 -v $(pwd):/site -w /site klakegg/hugo:0.107.0-ext-ubuntu-onbuild server --bind=0.0.0.0
```

[Runners Gitlab](https://docs.gitlab.com/runner/install/): shared-runners GitLab ($$), ou vos propres runners selon votre préférence.


## Implémentation niveau 1 (8/20) - Mise en place de Gitlab-CI
___
- [Gitflow](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow#:~:text=Qu'est%2Dce%20que%20Gitflow,Vincent%20Driessen%20de%20chez%20nvie.) à implémenter: une branche principale et des branches de features


    ![gitflow](./doc/gitflow.png)
    Schema 2 : Gitflow


- 2 stages (see : [gitlab-ci](./.gitlab-ci.yml))

    1. build : se déclenche uniquement sur une `branche`, génère le contenu statique et le stocke comme artefact pour une durée défini.
   > ⚠️ Le repertoire themes contient des sous-modules git (les thèmes Hugo, qui gèrent l'affichage du contenu).
    Par défaut, les sous-modules ne sont pas présents dans le runner gitlab. Hint: regardez la doc gitlab sur les variables `GIT_SUBMODULE_STRATEGY` et `GIT_SUBMODULE_DEPTH`
    
    2. deploy  : se déclenche pour un `tag` respectant la sémantique de version [semver](https://semver.org/lang/fr).
    * Si je suis dans un contexte de `merge request`, je déploie le site sur un environnement de **déploiement**
    * Si je suis dans un contexte de `tag`, je déploie le site dans l'environnement de **production**.


### A ce stade, on doit avoir:
* Implémentation du gitflow de base : pipelines Build / Deploy executés en fonction des actions des contributeurs.
* site web de prod déployé sur un bucket s3 servi par un CDN.
* site web "sandbox" de développement déployé sur un second bucket s3, également servi par un CDN.


---

## Implémentation niveau 2 (12/20) - Pour aller plus loin

* Gérer plusieurs environnements de développement en parallèle
* Cycle de vie des infrastructures: après 24h, destruction des site web "sandbox" de développement

---

## Implémentation niveau 3 (X/20) - Faites nous plaisir :)
* Implémentation de l'infra en IAC (Terraform)
* Ajout d'un CMS
* Hébergement des images sur un bucket S3 à part
* gitflow++: ajout d'un stage `approve` dans les pipeline qui demande l'approval d'un admin avant d'effectuer la mise en production.
* Ajout de features serverless: formulaire de contact, inscription à une newsletter, ... Changez le thème Hugo au besoin.

![jam](./doc/jamstack.png)

# Workflow utilisateur type (comment l'exercice sera validé):

* En tant que contributeur, j'effectue une modification sur un [fichier markdown du site](./content/_index.md). Je commit ma modification, la push sur une branche `feature/X` et ouvre une merge request vers master.
* Le pipeline de build se lance, construit mon site web. Le pipeline d'export prend la suite et exporte mon site web vers un environnement de développement.
* Je consulte mon site sur l'environnement de développement pour valider les modifications.
* Validation de la merge request. Merge sur master.
* Je push sur master un nouveau tag de release. Ex: `1.0.0`
* Build & export du site vers l'environnement de production via les pipelines
* Je constate les modifications sur le site de production


# Objectif du TP

* Découvrir une nouvelle approche du développement Web & serverless.
* Vous confronter aux problématique de pipeline automation, de versioning, d'environnements.
* Vous donner un exemple concret d'application du SD², pour un site web moins couteux pour vous comme pour la planète.